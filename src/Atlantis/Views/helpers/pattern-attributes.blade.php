
<div class="row">
	<div class="column large-{{ $attr['columns'] ?? 12 }}"><a href="javascript:void(0)" data-open="preview_image_path">Screenshot</a></div>	
	@foreach($config['attributes'] as $k => $attr)

	<div class="column large-{{ $attr['columns'] ?? 12 }}">
		<label for="">{{ $attr['name'] }}</label>

		<input type="hidden" name="attr[{{ $k }}][name]" value="{{ $attr['key'] }}">

		<?php $current_val = $oFields->first(function($item) use($attr) {
			return $item->key == $attr['key'];
		}); ?>

		@switch($attr['type'])
			@case('text')
				<input type="text" name="attr[{{ $k }}][value]" value="{{ $current_val->value ?? $attr['default_value'] }}">
			@break

			@case('textarea')
				<textarea name="attr[{{ $k }}][value]" rows="8">{{ $current_val->value ?? $attr['default_value'] }}</textarea>
			@break

			@case('option')
				{!! Form::select('attr['.$k.'][value]', $attr['options'], $current_val->value ?? $attr['default_value'] ) !!}
			@break

			@case('images')
				{!! MediaTools::createImageSelector(NULL, true,
					is_null($current_val) ? $attr['default_value'] : (is_array($current_val->value) ? $current_val->value : [$current_val->value] ) ,
					false,
					'attr['.$k.'][value][]') !!}
			@break

			@case('image')
				{!! MediaTools::createImageSelector(NULL, false, [$current_val->value ?? $attr['default_value']], false, 'attr['.$k.'][value]') !!}
			@break

			@case('radio')
				@foreach($attr['options'] as $value => $label)
					<input {{ $current_val->value == $value ? 'checked' : ''}} type="radio" id="{{ \Illuminate\Support\Str::slug($value) }}" name="attr[{{ $k }}][value]" value="{{ $value }}">
  					<label style="font-family:Open sans,Helvetica Neue,Helvetica,Roboto,Arial,sans-serif !important;text-transform: initial;font-weight: 400;" for="{{ \Illuminate\Support\Str::slug($value) }}">{!! $label !!}</label><br>
				@endforeach
			@break

			@default
				<span>Something went wrong, please try again</span>
		@endswitch

		<p class="help-text">
			{{ $attr['help_text'] ?? ''}}
		</p>


	</div>

	@endforeach
</div>
<div class="reveal" id="preview_image_path" data-reveal>
  <img src="{{ config('atlantis.theme_path') . '/' . $config['preview_image_path'] }}">
</div>