<?php

namespace Atlantis\Helpers;

class PLUploadReceiver {

  public function upload($subDir = false) {

    $targetDir = config('atlantis.user_media_upload') . ($subDir ? $subDir : '');

    $cleanupTargetDir = true;

    $maxFileAge = 5 * 3600;

    //$file = \Input::file('file')->move($targetDir, \Input::file('file')->getClientOriginalName());
    //die('{"jsonrpc" : "2.0", "result" : null, "id" : 2}');

    $original_name = request()->get('name');

    $filePath = $targetDir . $original_name;

    $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
    $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

    if ($cleanupTargetDir) {
      if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
        
        mkdir(base_path() . '/' . $targetDir, 0755);

        if (!$dir = opendir($targetDir)) {
    
          header('HTTP/1.1 500');
    
          die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');

        }
      }

      while (($file = readdir($dir)) !== false) {

        $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

        // If temp file is current file proceed to the next
        if ($tmpfilePath == "{$filePath}.part") {
          continue;
        }

        // Remove temp file if it is older than the max age and is not the current file
        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
          @unlink($tmpfilePath);
        }
      }

      closedir($dir);
    }

    // Open temp file
    if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
      header('HTTP/1.1 500');
      die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
    }

    if (!empty($_FILES)) {
      if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
        header('HTTP/1.1 500');
        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
      }

      // Read binary input stream and append it to temp file
      if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
        header('HTTP/1.1 500');
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
      }
    } else {
      if (!$in = @fopen("php://input", "rb")) {
        header('HTTP/1.1 500');
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
      }
    }

    while ($buff = fread($in, 4096)) {
      fwrite($out, $buff);
    }

    @fclose($out);
    @fclose($in);

    // Check if file has been uploaded
    if (!$chunks || $chunk == $chunks - 1) {

      $fname = time() . '-' . str_replace(' ', '_', $original_name);

      // Strip the temp .part suffix off
      rename("{$filePath}.part", $targetDir . $fname);
      
      return $subDir ? $subDir . $fname : $fname;


      // Return Success JSON-RPC response
      //return '{"jsonrpc" : "2.0", "target_name" : "' . $fname . '", "id" : "' . $id . '"}';
    }
  }

}
